# Launch Style Guide

This is an unofficial style guide for Adobe Launch, inspired by style guides created for other frameworks (e.g. [Vue](https://vuejs.org/v2/style-guide/)).

Launch is a powerful platform that provides lots of openness and flexibility, which leaves plenty of opportunities for confusion. This style guide is meant to catalog best practices to foster smooth collaboration with teams, easy comprehension for technical and non-technical users alike, and provide standards to address common use cases.

## Rule Categories

### Priority A Rules: Essential
These rules help prevent errors, so learn and abide by them at all costs. Exceptions may exist, but should be very rare and only be made by those with expert knowledge of both JavaScript and Launch.

### Priority B Rules: Strongly Recommended
These rules have been found to improve readability and/or developer experience in most projects. Your code will still run if you violate them, but violations should be rare and well-justified.

### Priority C Rules: Recommended
Where multiple, equally good options exist, an arbitrary choice can be made to ensure consistency. In these rules, we describe each acceptable option and suggest a default choice. That means you can feel free to make a different choice in your own property, as long as you’re consistent and have a good reason. Please do have a good reason though! By adapting to the community standard, you will:

1. train your brain to more easily parse most of the community code you encounter
2. be able to copy and paste most community code examples without modification
3. often find new hires are already accustomed to your preferred coding style, at least in regards to Launch

### Priority D Rules: Use with Caution
Some features of Launch exist to accommodate rare edge cases or smoother migrations from a legacy code base. When overused however, they can make your code more difficult to maintain or even become a source of bugs. These rules shine a light on potentially risky features, describing when and why they should be avoided.

## Priority A: Essential

### Action sequencing

- Only use if required; otherwise leave disabled
- If checked, have to uncheck wait 2000ms before executing next rule

## Priority B: Strongly Recommended

### Property names

### Limit number of properties
Use as few properties as possible. The more properties you have, the harder it is to maintain your implementation.

### Use as few rules as possible
As the number of rules increases, so does the size of your build artifact and the processing time required on the client side. Use only the minimum number of rules necessary to implement consistent tagging.

### Minimize use of custom code
This may seem counterintuitive to the above, but custom code should be used in cases where the Launch UI does not provide functionality to achieve the desired result. Launch was designed to be accessible to marketers and developers, so assume that whatever you implement will need to be understood quickly by someone who doesn't know JavaScript.

### Use Custom Event instead of Direct Call where possible

### Use consistent naming conventions

### Rule names

- `all lowercase | AA`

### Event names

- rename event names for **Data Layer Manager - Data Layer Push** to **DLM:** <eventName>

### Action names

## Priority C: Recommended

### Update extensions regularly

- have a process for keeping extensions up to date

### Adobe Analytics

- don't expose tracker object unless a script outside of Launch requires it
- don't enable activity map unless client uses it
- use Common Analytics Plugins extension instead of custom tracker code/doPlugins
- use Clear Variables action *after* fire beacon, not before Set Variables
  - if that's not possible, be consistent: always clear before set vars or always clear after firing beacon

## Priority D: Use with caution

### Running scripts in rule conditions
Sometimes you will need to evaluate some code immediately after a rule is triggered, but custom code actions aren't execute immediately. In this case, you can run custom code right away inside a custom code condition, as long as the expression returns true.

For example, you can grab link text immediately after the rule is triggered and set it to a dynamic data element, which can be used in actions that are executed on the same rule.

```javascript
var linkName = event.element.innerText;
_satellite.setVar('dyn_linkName', linkName);

// Make sure to return true!
return true;
```